from flask import Flask, request
from waitress import serve
import os
import slack
import time
import hmac
from hashlib import sha256
from threading import Thread
import requests

token = os.environ["SLACK_TOKEN"]
slack_signing_secret = os.environ["SLACK_SIGNING_SECRET"]

wc = slack.WebClient(token=token)


def wcwrapper(wc_func, **kwargs):
    try:
        return wc_func(**kwargs)
    except slack.errors.SlackApiError as err:
        if "ratelimited" in err.args[0]:
            time.sleep(10)
            return wcwrapper(wc_func, **kwargs)


def join_channel(chan):
    return wcwrapper(wc.conversations_join, channel=chan)


def post_message(chan, msg):
    join_channel(chan)
    wcwrapper(wc.chat_postMessage, channel=chan, text=msg)


def get_topic(chan):
    topic = wcwrapper(wc.conversations_info, channel=chan)["channel"]["topic"]["value"]
    return topic


def invite_all_members(chan):
    join_channel(chan)
    userlist = wc.users_list()
    idlist = [
        member["id"]
        for member in userlist["members"]
        if member["name"] != "puzzelbeheerder"
    ]
    wcwrapper(wc.conversations_invite, channel=chan, users=idlist)


def set_topic(chan, text):
    join_channel(chan)
    wcwrapper(wc.conversations_setTopic, channel=chan, topic=text)


def rename_channel(chanid, newname):
    join_channel(chanid)
    wcwrapper(wc.conversations_rename, channel=chanid, name=newname)


def cleanup(prefix="opg"):
    convolist = wcwrapper(wc.conversations_list, exclude_archived=True, limit=300)
    convidlist = [
        channel["id"] for channel in convolist["channels"] if prefix in channel["name"]
    ]
    for convid in convidlist:
        join_channel(convid)
        wcwrapper(wc.conversations_archive, channel=convid)


def topic_append(chan, text):
    old_topic = get_topic(chan)
    new_topic = old_topic + " " + text
    set_topic(chan, new_topic)


def create_list(prefix, aantal, postfix, response_url):
    for idx in range(1, aantal + 1):
        channameids = f"{idx:02}"
        channame = prefix.rstrip("_") + "_" + channameids + "_" + postfix.lstrip("_")
        create_one(channame)
    feedback(response_url, "Kanalen aangemaakt!")


def get_user_realname(username):
    userlist = wc.users_list()
    members = {member["name"]: member["real_name"] for member in userlist["members"]}
    return members[username]


def create_one(channame, response_url=None):
    chandata = wc.conversations_create(name=channame)
    chanid = chandata["channel"]["id"]
    invite_all_members(chanid)
    if response_url:
        feedback(response_url, f"{channame} aangemaakt!")


def validate_request(rq):
    timestamp = rq.headers["X-Slack-Request-Timestamp"]
    if abs(time.time() - int(timestamp)) > 60 * 2:
        return False
    version_number = "v0"
    rqdata = rq.get_data().decode("utf-8")
    sig_basestring = ":".join([version_number, timestamp, rqdata])
    computed_sig = hmac.new(
        slack_signing_secret.encode("utf-8"),
        sig_basestring.encode("utf-8"),
        digestmod=sha256,
    ).hexdigest()
    computed_sig = f"v0={computed_sig}"
    slack_sig = request.headers["X-Slack-Signature"]
    return computed_sig == slack_sig


def feedback(response_url, msg):
    requests.post(response_url, json={"text": msg, "type": "ephemeral"})


def say_hello(tgt_chan, rq_user, rq_text, response_url):
    msg = "{} vroeg mij jullie te groeten met de woorden: {}".format(
        get_user_realname(rq_user), rq_text
    )
    post_message(tgt_chan, msg)
    feedback(response_url, "Groet geplaatst!")


def maakoplossing(tgt_chan, tgt_chan_name, rq_text, rq_user, response_url):
    topic = get_topic(tgt_chan)
    if rq_text == "":
        pass
    elif topic == "" or rq_text.split()[0] == "overwrite":
        if rq_text.split()[0] == "overwrite":
            rq_text = " ".join(rq_text.split()[1:])
        set_topic(tgt_chan, rq_text)
    elif rq_text.split()[0] == "append":
        rq_text = " ".join(rq_text.split()[1:])
        topic_append(tgt_chan, rq_text)
    else:
        feedback(
            response_url,
            "Er staat al een oplossing in de tekst. Specificeer 'overwrite' "
            "of 'append' om te overschrijven of samen te voegen. Of geef het "
            "commando zonder verdere tekst om enkel het kanaal te hernoemen.",
        )
    final_topic = get_topic(tgt_chan)
    if final_topic == "":
        feedback(
            response_url, "De oplossing is leeg. Dat lijkt me geen valide oplossing."
        )
    if tgt_chan_name[:4] != "zzzz":
        new_channel_name = "zzzz_" + tgt_chan_name + "_opgelost"
        rename_channel(tgt_chan, new_channel_name)
    msg = ":tada: :confetti_ball: {} heeft dit kanaal gemarkeerd als opgelost. De oplossing luidt: *{}*".format(
        get_user_realname(rq_user), final_topic
    )
    post_message(tgt_chan, msg)
    feedback(response_url, "Gelukt! Gefeliciteerd met de oplossing!!")


app = Flask(__name__)


@app.route("/hello", methods=["POST"])
def hello():
    if not validate_request(request):
        return "Invalid request"
    tgt_chan = request.form["channel_id"]
    rq_user = request.form["user_name"]
    rq_text = request.form["text"]
    response_url = request.form["response_url"]
    hallothread = Thread(
        target=say_hello, args=(tgt_chan, rq_user, rq_text, response_url)
    )
    hallothread.start()
    return "Verzoek wordt uitgevoerd!"


@app.route("/losop", methods=["POST"])
def losop():
    if not validate_request(request):
        return "Invalid request"
    tgt_chan = request.form["channel_id"]
    tgt_chan_name = request.form["channel_name"]
    rq_text = request.form["text"]
    rq_user = request.form["user_name"]
    response_url = request.form["response_url"]
    oplosthread = Thread(
        target=maakoplossing,
        args=(tgt_chan, tgt_chan_name, rq_text, rq_user, response_url),
    )
    oplosthread.start()
    return "Verzoek ontvangen. Ik ga ermee aan de slag!"


@app.route("/deeloplossing", methods=["POST"])
def deeloplossing():
    if not validate_request(request):
        return "Invalid request"
    tgt_chan = request.form["channel_id"]
    rq_text = request.form["text"]

    purpthread = Thread(target=topic_append, args=(tgt_chan, rq_text))
    purpthread.start()
    return f"Deeloplossing {rq_text} toegevoegd aan kanaalomschrijving! :tada: :confetti_ball:"


@app.route("/inviteall", methods=["POST"])
def inviteall():
    if not validate_request(request):
        return "Invalid request"
    tgt_chan = request.form["channel_id"]
    inviter = Thread(target=invite_all_members, args=(tgt_chan,))
    inviter.start()
    return "Verzoek ontvangen, ik ga ermee aan de slag!"


@app.route("/cleanup", methods=["POST"])
def cleanup_from_call():
    if not validate_request(request):
        return "Invalid request"
    prefix = request.form["text"]
    if prefix == "":
        cleaner = Thread(target=cleanup)
    else:
        cleaner = Thread(target=cleanup, args=(prefix,))
    cleaner.start()
    return "Verzoek ontvangen! Ik ga lekker opruimen!"


@app.route("/maak", methods=["POST"])
def maakkanaal():
    if not validate_request(request):
        return "Invalid request"
    channame = request.form["text"]
    response_url = request.form.get("response_url")
    if channame == "" or " " in channame:
        return "Kanaal moet één naam hebben, niet nul of meer dan 1!"
    chancrea = Thread(target=create_one, args=(channame, response_url))
    chancrea.start()
    return "Aanmaak aangezet"


@app.route("/maaklijst", methods=["POST"])
def maakkanaallijst():
    if not validate_request(request):
        return "Invalid request"
    chandata = request.form["text"]
    response_url = request.form.get("response_url")
    if chandata == "":
        return "Specificeer pre en postfix voor kanaalnamen"
    pars = chandata.split()
    if len(pars) != 3:
        return "Je moet een aantal, een prefix en een postfix specificeren"
    try:
        aantal = int(pars[0])
    except ValueError:
        return "Aantal moet een getal zijn"
    prefix = pars[1]
    postfix = pars[2]

    chansthr = Thread(target=create_list, args=(prefix, aantal, postfix, response_url))
    chansthr.start()
    return "Verzoek ontvangen, ik ga ermee aan de slag!"


@app.route("/help", methods=["POST"])
def bothelp():
    if not validate_request(request):
        return "Invalid request"
    helptekst = """
    Hallo, ik ben Puzzelbot. Ik implementeer een aantal huishoudelijke functies in deze slack space.
    Ik ondersteun de volgende slash commandos (die je in de text entry van een kanaal kunt invoeren):
    /sayhi <begroeting> - Ik zal het kanaal begroeten, namens jou met de als parameter meegegeven tekst als groet (dit is vooral een handige testfunctie die niet veel doet).
    /deeloplossing <oplossingstekst> Geef een deeloplossing. Ik zal dan de deeloplossing aan de omschrijving van het kanaal toevoegen.
    /losop [append, overwrite] [oplossingstekst] - Markeer het kanaal als opgelost. Als je een tekst als argument meegeeft én het kanaal heeft nog geen oplossing, dan zal die tekst als oplossing worden neergezet. Als er wel een oplossing staat en je geeft een tekst als argument mee, moet je kiezen of je de bestaande oplossing wilt overschrijven (voeg het woord "overwrite" toe aan het begin van de tekst), of achteraan wilt bijvoegen (voeg het woord "append" toe aan het begin van de tekst). Geef je dit commando zonder argument, dan wordt de huidige oplossing niet aangepast en het kanaal als opgelost gemarkeerd (NB: Ik accepteer geen lege oplossingen!).  
    /maakkanaal <kanaalnaam> - Maak een kanaal met de naam <kanaalnaam> en nodig iedereen uit om er lid van te worden
    /maaklijst aantal <prefix> <postfix> - Maak kanalen aan die oplopend genummerd zijn van 1 tot en met <aantal> en de naam <prefix>_<00>_<postfix> hebben met <00> het tweecijferig rangtelnummer met eventueel een voorloopnul. Nodig alle gebruikers uit in deze kanalen. 
    /cleanup <selectie> - Archiveer alle kanalen waar de tekst <selectie> in de naam staat. Door de limieten van de slack API kan ik alleen opruimen als ik de kanalen ook zelf heb aangemaakt. Let even op dat je de selectietekst niet te kort maakt want dan verwijder je per ongeluk misschien #general of #random (dat heeft Yan echt nooit gedaan hoor ;))! 
    /iedereen - Als je handmatig een kanaal hebt angemaakt kun je dit commando gebruiken om alle gebruikers uit te nodigen voor dit kanaal.
    /bothelp - Moet ik die uitleggen? Volgens mij heb je die net ingetikt :) 
    Dat was het :tada:. Ik hoop je nog wel eens van dienst te kunnen zijn!

   Let op! Ik doe niet aan authorisatie. Je kunt dus alles doen via mij, onafhankelijk van wie je bent. 
    """
    return helptekst


@app.route("/blijfwakker", methods=["GET"])
def blijfwakker():
    return "Ik blijf wakker!"


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 4443))
    serve(app, host="0.0.0.0", port=port)
